#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
#include <conio.h>
#include "UDPClient.h"
//#include "highgui.h"
//#include "cv.h"
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "opencv2/opencv.hpp"
//#include "opencv_modules.hpp"
using namespace std;
using namespace cv;



int UDPMAX = (145 * 150);
int port = 9999;

int main() {
	cout << "=== VIDEO RECEIVER ===" << endl;

	//setup UDP client
	UDPClient *client = new UDPClient(port);
	char *buff = (char*)malloc(UDPMAX * 3);
	Mat image, image2;
	Mat left;
	Mat right;
	bool switch1 = false;
	left = Mat::zeros(145, 150, CV_8UC3);
	right = Mat::zeros(145, 150, CV_8UC3);

	image = Mat::zeros(145, 300, CV_8UC3);
	image2 = Mat::zeros(480, 480, CV_8UC3);
	uchar *iptr = image.data;
	int imgSize = left.total() * left.elemSize();

	uchar *iptrr = right.data;
	uchar *iptrl = left.data;
	cout << imgSize << endl;
	Mat read1(image, Rect(0, 0, 150, 145));
	Mat read2(image, Rect(150, 0, 150, 145));

	if (!image.isContinuous())
	{
		image = image.clone();
		image2 = image.clone();
	}
	if (!left.isContinuous()) {
		left = left.clone();
	}
	if (!right.isContinuous()) {
		right = right.clone();
	}
	cout << "here" << endl;
	Size size(150, 145);
	while (1) {
		//read data
		int result = client->receiveData((char*)buff, (UDPMAX * 3));
		if (result < 0) {
			cout << "Failed to receive frame." << endl;
			continue;
		}
		//cout << "Got a frame of size " << result << endl;
		if (switch1)
		{
			//videoBuffer.resize(result);
			memcpy((char*)iptrr, buff, result);
			//cv::imshow("CV Video R Client", right);
			switch1 = !switch1;
		}
		else
		{
			memcpy((char*)iptrl, buff, result);
			//cv::imshow("CV Video L Client", left);
			switch1 = !switch1;
		}
		left.copyTo(read1);
		right.copyTo(read2);

		imshow("UDP Video Receiver1", image);
		resize(image, image2, Size(480, 480), 0, 0, INTER_CUBIC);
		imshow("UDP Video Receiver2", image2);
		cvWaitKey(5);
	}
}