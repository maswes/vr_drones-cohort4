#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "opencv2/opencv.hpp"
#include "opencv2/opencv_modules.hpp"


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "12345"
using namespace std;
using namespace cv;

int __cdecl main(int argc, char **argv)
{
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	char *sendbuf = "this is a test";
	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;

	// Validate the parameters
	if (argc != 2) {
		printf("usage: %s server-name\n", argv[0]);
		return 1;
	}

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}

	// Send an initial buffer
	iResult = send(ConnectSocket, sendbuf, (int)strlen(sendbuf), 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	printf("Bytes Sent: %ld\n", iResult);

	// shutdown the connection since no more data will be sent
	iResult = shutdown(ConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	// Receive until the peer closes the connection

	/*		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
	if (iResult > 0)
	printf("Bytes received: %d\n", iResult);
	else if (iResult == 0)
	printf("Connection closed\n");
	else
	printf("recv failed with error: %d\n", WSAGetLastError());*/



	//----------------------------------------------------------
	//OpenCV Code
	//----------------------------------------------------------

	Mat img;
	Mat left;
	Mat right;
	bool switch1 = false;
	left = Mat::zeros(240, 320, CV_8UC4);
	right = Mat::zeros(240, 320, CV_8UC4);

	img = Mat::zeros(240, 640, CV_8UC4);
	//int imgSize = img.total() * img.elemSize();
	int imgSize = left.total() * left.elemSize();
	uchar *iptrr = right.data;
	uchar *iptrl = left.data;

	int bytes = 0;
	int key = 0;

	Mat read1(img, Rect(0, 0, 320, 240));
	Mat read2(img, Rect(320, 0, 320, 240));
	//make img continuos
	if (!img.isContinuous()) {
		img = img.clone();
	}

	if (!left.isContinuous()) {
		left = left.clone();
	}
	if (!right.isContinuous()) {
		right = right.clone();
	}
	std::cout << "Image Size:" << imgSize << std::endl;


	namedWindow("CV Video Client", 1);

	while (key != 'q') {

		if (switch1)
		{
			if ((bytes = recv(ConnectSocket, (char *)iptrl, imgSize, MSG_WAITALL)) == -1) {
				std::cerr << "recv failed, received bytes = " << bytes << std::endl;
			}
			switch1 = !switch1;
			// cv::imshow("CV Video Client", left);
		}
		else
		{
			if ((bytes = recv(ConnectSocket, (char *)iptrr, imgSize, MSG_WAITALL)) == -1) {
				std::cerr << "recv failed, received bytes = " << bytes << std::endl;
			}
			switch1 = !switch1;
			//cv::imshow("CV Video Client", right);
		}
		//Mat read1(img, Rect(0, 0, 320, 240));
		//Mat read2(img, Rect(320, 0, 320, 240));
		left.copyTo(read1);
		right.copyTo(read2);

		cv::imshow("CV Video Client", img);

		if (key = cv::waitKey(10) >= 0) break;
	}

	closesocket(ConnectSocket);
	WSACleanup();
	return 0;



	// cleanup

	return 0;
}