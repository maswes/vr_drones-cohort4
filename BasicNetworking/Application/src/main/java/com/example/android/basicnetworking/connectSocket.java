package com.example.android.basicnetworking;

import android.os.Bundle;
import android.app.Activity;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.android.basicnetworking.client.*;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.android.OpenCVLoader;
import java.io.File;

public class connectSocket extends Activity {
    static TextView response;
    EditText editTextAddress, editTextPort;
    Button buttonConnect, buttonClear, button_pic;
    ImageView imgview;

    static {
        if (!OpenCVLoader.initDebug()) {
            // Handle initialization error
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_socket);
        editTextAddress = (EditText) findViewById(R.id.addressEditText);
        editTextPort = (EditText) findViewById(R.id.portEditText);
        buttonConnect = (Button) findViewById(R.id.connectButton);
        buttonClear = (Button) findViewById(R.id.clearButton);
        response = (TextView) findViewById(R.id.responseTextView);
        button_pic = (Button) findViewById(R.id.button_pic);
        imgview = (ImageView) findViewById(R.id.imageView1);
        button_pic.setOnClickListener(onListener);
        buttonConnect.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //hardcode
                    /*
                    client myClient = new client(editTextAddress.getText()
                            .toString(), Integer.parseInt(editTextPort
                            .getText().toString()), response);
                            */
                String ipaddr = "192.168.253.100";
                int port = 12345;
                client myClient = new client(ipaddr,port,imgview);
                int SDK_INT = android.os.Build.VERSION.SDK_INT;
                boolean flag = false;
               /* if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_2,
                        connectSocket.this, mOpenCVCallBack)) {

                    Log.e("TEST", "Cannot connect to OpenCV Manager");
                } */
                if (SDK_INT > 8)
                {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                            .permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    //your codes here
                    myClient.execute(0);
                    if (myClient.bitmap != null)
                        imgview.setImageBitmap(myClient.bitmap);
                    response.setText(myClient.response);
                }
            }
        });
        buttonClear.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                response.setText("");
            }
        });
    }
    private OnClickListener onListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId())
            {
                // show pic button
                case R.id.button_pic:
                    show_pic();
                    break;
                default:
                    break;
            }
        }
    };
    private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    //your code

                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };
    private void show_pic()
    {


        String inputFile = "im1";
        String inputEx = "jpg";
        String inputDir = getCacheDir().getAbsolutePath();
        String outputDir = getCacheDir().getAbsolutePath();
        String outputEx = "png";
        String inputPath = inputDir + File.separator + inputFile + "." + inputEx;
        Log.d(this.getClass().getSimpleName(), "loading" + inputFile + "...");
        Mat image = Imgcodecs.imread(inputPath);
        Log.d("connectSocket", "Width" + inputFile + image.width());
        int threshold1 = 70;
        int threshold2 = 100;
        Mat im_canny = new Mat();

        Imgproc.Canny(image, im_canny, threshold1, threshold2);
        String outfile = outputDir + File.separator + "newIm" + "." + outputEx;
        Log.d("making img", "outputing now");
        Imgcodecs.imwrite(outfile, im_canny);

    }
}
