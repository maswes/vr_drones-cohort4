package com.example.android.basicnetworking;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.app.Activity;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import android.widget.ImageView;
import java.lang.Runnable;
/**
 * Created by zhujun on 2016/3/29.
 */
public class client extends AsyncTask<Integer, Void, Bitmap> {
    public Mat img;
    ImageView imgview;

    private int data = 0;
    String dstAddress;
    int dstPort;
    String response = "";
    private String message = "Hello Ravi";
    Socket socket;
    public Socket myClient = null;
    Bitmap bitmap;

    client(String addr, int port, ImageView imgview) {
        dstAddress = addr;
        dstPort = port;
        this.imgview = imgview;
        connectSocket.response.setText("client init");
    }
    @Override
    protected Bitmap doInBackground(Integer... params){
        try {
            Bitmap bm;
            data = params[0];
            socket = new Socket(dstAddress, dstPort);
            ByteArrayOutputStream  byteArrayOutputStream = new ByteArrayOutputStream(320 * 4 * 240);
            byte[] buffer = new byte[320 * 4 * 240];
            int bytesRead = 0;
            InputStream inputStream = socket.getInputStream();
            //read data
            while((bytesRead = inputStream.read(buffer)) != -1){
                byteArrayOutputStream.write(buffer, 0, bytesRead);
                byteArrayOutputStream.flush();
                img = Mat.zeros(320,240, CvType.CV_8UC4);
                int imgSize = (int)img.total() * (int)img.elemSize();
                if (!img.isContinuous())
                    img = img.clone();
                int nrOfPixels = buffer.length / 4;
                int pixels[] = new int[nrOfPixels];
                for (int i = 0; i < nrOfPixels; i++)
                {
                    int r = buffer[4*i];
                    int g = buffer[4*i+ 1];
                    int b = buffer[4 * i + 2];
                    int a = buffer[4 * i + 3];
                    if (r < 0)
                        r = r + 256;
                    if (g < 0)
                        g = g + 256;
                    if (b < 0)
                        b = b + 256;
                    if (a < 0)
                        a = a + 256;
                    pixels[i] = Color.argb(a,r,g,b);


                }
                bitmap = Bitmap.createBitmap(pixels, 320, 240, Bitmap.Config.ARGB_8888);
                //imgview.setImageBitmap(bitmap);
                //reset buffer

                byteArrayOutputStream.reset();
                response += byteArrayOutputStream.toString("UTF-8");
            }

        }catch(UnknownHostException e){
            e.printStackTrace();
            response = "UnknownHostException: " + e.toString();

        }catch(IOException e){
            e.printStackTrace();
            response = "IOException: " + e.toString();
        }finally
        {
            if (socket != null)
                try {
                    socket.close();
                }catch(IOException e){
                    e.printStackTrace();
                }

        }
        return bitmap;
    }
    @Override
    protected void onPostExecute(Bitmap bm){
        if (bm != null)
            imgview.setImageBitmap(bm);
    }

}
