package com.example.android.basicnetworking;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;

import com.example.android.common.logger.Log;
import com.example.android.basicnetworking.MainActivity;
import android.net.wifi.p2p.WifiP2pManager.Channel;

/**
 * Created by zhujun on 2016/3/27.
 */
public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {
    public static final String TAG = "Basic Network Demo";
    private WifiP2pManager manager;
    private Channel channel;
    //activity associated with the receiver
    private Activity activity;
    public WiFiDirectBroadcastReceiver(WifiP2pManager manager, Channel channel,
                                       Activity activity) {
        super();
        this.manager = manager;
        this.channel = channel;
        this.activity = activity;
    }
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.i(TAG,action);
        if(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action))
        {
            if (manager == null) {
                return;
            }
                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
            if (networkInfo.isConnected()){
                // we are connected with the other device, request connection
                // info to find group owner IP
                Log.i(TAG,"Connected to p2p network. Requesting network details");
                manager.requestConnectionInfo(channel, (WifiP2pManager.ConnectionInfoListener)activity);
            }
            else {
                // disconnect
            }

        }else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)){
            WifiP2pDevice device = (WifiP2pDevice)intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
            Log.i(TAG, "Device status = " + device.status);
        }

    }
}
