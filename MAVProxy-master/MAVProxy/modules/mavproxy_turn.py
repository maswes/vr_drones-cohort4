#!/usr/bin/env python
'''turn command handle yaw'''
import time, math, os
from pymavlink import mavutil

from MAVProxy.modules.lib import mp_module

class TurnModule(mp_module.MPModule):
    def _init_(self, mpstate):
        super(TurnModule, self).__init__(mpstate, "yaw", "yaw control", public=True)
        self.add_command('yaw_left', self.yaw_left, "yaw left")
        self.add_command('yaw_right', self.yaw_right, "yaw right")

    def yaw_left(self, args):
        ''''yaw_left angle'''
    #<p1(target angle: [0-360], 0 is north)|p2(speed during yaw change:[deg per second])|p3 direction: negative: counter clockwise, positive: clockwise [-1,1]
        usage = "usage: yaw left | angle"
        if len(args) != 1:
            print(usage)
            return
        angl == float(args[0])
        self.master.mav.command_long_send(
            self.settings.target_system,
            mavutil.mavlink.MAV_COMP_ID_SYSTEM_CONTROL,
            mavutil.mavlink.MAV_CMD_CONDITION_YAW, # command
            0, # confirmation
            angle, # param1 (angle value)
            1, # param2 (angular speed value)
            -1,  # param3 (direction)
            0,  # param4 (mode: 0->absolute / 1->relative)
            0,  # param5
            0,   # param6
            0)  # param7
        return
   
    def yaw_right(self, args):
        ''''yaw_right command'''
        #<p1(target angle: [0-360], 0 is north)|p2(speed during yaw change:[deg per second])|p3 direction: negative: counter clockwise, positive: clockwise [-1,1]
        usage = "usage: yaw right | angle"
        if len(args) != 1:
            print(usage)
            return
        angl == float(args[0])
        self.master.mav.command_long_send(
                    self.settings.target_system,
                    mavutil.mavlink.MAV_COMP_ID_SYSTEM_CONTROL,
                    mavutil.mavlink.MAV_CMD_CONDITION_YAW, # command
                    0, # confirmation
                    angle, # param1 (angle value)
                    1, # param2 (angular speed value)
                    0,  # param3 (direction)
                    0,  # param4 (mode: 0->absolute / 1->relative)
                    0,  # param5
                    0,  # param6
                    0)  # param7
        return

    def init(mpstate):
        '''initialise module'''
        return TurnModule(mpstate) 
        

